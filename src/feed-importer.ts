/*
 * Copyright (C) 2019-2022 Open StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {SCMessage} from '@openstapps/core';
import fetch from 'node-fetch';
import {FetchError} from 'node-fetch';

import FeedParser = require('feedparser');

const HTTP_STATUS_OK = 200;

/**
 * FeedImporter Class
 */
export class FeedImporter {
  /**
   * Property to store objects fetched from the feed
   */
  private readonly items: object[] = [];

  /**
   * URL of the feed
   */
  private readonly url: string;

  constructor(url: string) {
    this.url = url;
  }

  /**
   * Removes properties of object depending on the property name
   */
  private filterFields(item: object, filter: string[]) {
    const filtered = Object.keys(item)
      .filter(key => filter.includes(key))
      // eslint-disable-next-line unicorn/no-array-reduce
      .reduce((object, key) => {
        return {
          ...object,
          // eslint-disable-next-line @typescript-eslint/no-explicit-any
          [key]: (item as any)[key],
        };
      }, {});

    return filtered;
  }

  /**
   * Returns items as objects
   */
  asObjects(): object[] {
    return this.items;
  }

  /**
   * Returns items as SCMessages
   */
  asSCMessages(transform: (feedItem: object) => SCMessage): SCMessage[] {
    const messages: SCMessage[] = [];

    for (const item of this.asObjects()) {
      messages.push(transform(item));
    }

    return messages;
  }

  /**
   * Provides items of the feed
   *
   */
  async getItems(filter: string[] = []): Promise<void> {
    return new Promise((resolve, reject) => {
      this.items.length = 0;
      fetch(this.url)
        .then(response => {
          if (response.status !== HTTP_STATUS_OK) {
            reject(new Error(`Rejecting response with statuscode: ${response.status}`));
          }

          const feedparser: FeedParser = new FeedParser({addmeta: false});
          let item: object | null;

          feedparser.on('readable', () => {
            try {
              item = feedparser.read();
              if (item !== null) {
                if (filter.length > 0) {
                  item = this.filterFields(item, filter);
                }
                this.items.push(item);
              }
            } catch (error) {
              reject(error);
            }
          });
          feedparser.on('end', () => {
            resolve();
          });
          feedparser.on('error', (error: Error) => {
            reject(error);
          });

          /* istanbul ignore next */
          response.body?.pipe(feedparser);
        })
        .catch((error: FetchError) => {
          reject(new Error(error.message));
        });
    });
  }
}
