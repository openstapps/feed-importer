# @openstapps/feed-importer

It's a library (npm package) which makes fetching data of an Atom or RSS feed easier.

In it's core, this is a wrapper for Atom/RSS parsing library [feedparser](https://github.com/danmactough/node-feedparser).

Compared to `feedparser`, this libary is much simpler to use, in a sense that a developer just needs to provide the URL of a feed, and he is already able to get that feeds items as an array of JavaScript objects.

## Installation

It's installed easily via npm:

```bash
npm install @openstapps/feed-importer
```

## Use

After `FeedImporter` class is imported, e.g.:

```javascript
import {FeedImporter} from '@openstapps/feed-importer';
```

an object needs to be created, and afterwards we are ready to use its `getItems` method:

```javascript
const feedImporter = new FeedImporter('http://some-url.domain'); // create an object

await feedImporter.getItems();
feedImporter.asObjects();
// do something with the items from the feed

await feedImporter.getItems(['title', 'description']);
// after fetching from the feed, items are filtered to contain only 'title' and 'description' properties

feedImporter.asSCMessages((feedItem: any): SCMessage => { ... });
// optionally transform the feed items to a SCMessage depending on your needs.
```

## Development

If you want to further develop this library or to just see how it works, these are useful commands:

```bash
npm install
npm run build
```

For running tests:

```bash
npm run test # or npm test
```
