/*
 * Copyright (C) 2018-2021 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {expect} from 'chai';
import {FeedImporter} from '../src/feed-importer';
import {SCMessage, SCThingType, SCThingOriginType} from '@openstapps/core';
/* eslint-disable jsdoc/require-jsdoc */

let feedImporter: FeedImporter;

async function checkErrorExpectations(url: string): Promise<void> {
  const afeedImporter = new FeedImporter(url);
  let resolved = false;
  let rejected = false;
  let error: Error;
  try {
    await afeedImporter.getItems();
    resolved = true;
  } catch (fetchError) {
    error = fetchError as Error;
    rejected = true;
  }
  expect(resolved).to.be.false;
  expect(rejected).to.be.true;
  expect(error!).to.be.an('error');
}

describe('FeedImporter Test Run', function () {
  this.timeout(10_000);
  this.slow(5000);

  before(() => {
    feedImporter = new FeedImporter('https://aktuelles.uni-frankfurt.de/feed/');
  });

  it('should get objects from an online feed', async () => {
    await feedImporter.getItems();
    expect(feedImporter.asObjects()).not.to.have.lengthOf(0);
    feedImporter.asObjects().forEach((item) => {
      expect(item).to.be.a('object');
    });
  });

  it('should get filtered objects from an online feed', async () => {
    await feedImporter.getItems(['title', 'description', 'pubDate']);
    expect(feedImporter.asObjects()).not.to.have.lengthOf(0);
    feedImporter.asObjects().forEach((item) => {
      expect(Object.keys(item)).to.have.same.members(['title', 'description', 'pubDate']);
    });
  });

  it('should apply custom transformation from feed object to SCMessage', async () => {
    await feedImporter.getItems();
    const transformedMessages = feedImporter.asSCMessages((feedItem: any): SCMessage => {
      const message: SCMessage = {
        audiences: [],
        categories: [],
        messageBody: feedItem.description,
        name: feedItem.title,
        origin: {
          indexed: new Date().toISOString(),
          name: 'test runner',
          type: SCThingOriginType.Remote,
        },
        type: SCThingType.Message,
        uid: '123456',
      };

      return message;
    });
    expect(transformedMessages).not.to.have.lengthOf(0);
    transformedMessages.forEach((item) => {
      expect(item).to.be.a('object');
    });
  });

  it('should reject with bad url', async () => {
    await checkErrorExpectations('http://non-existing-url.domain');
  });

  it('should reject with non 200 status code response', async () => {
    await checkErrorExpectations('https://aktuelles.uni-frankfurt.de/non-existing');
  });
});
