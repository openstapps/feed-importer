# [1.3.0](https://gitlab.com/openstapps/feed-importer/compare/v1.2.0...v1.3.0) (2022-08-23)



# [1.2.0](https://gitlab.com/openstapps/feed-importer/compare/v1.1.0...v1.2.0) (2022-03-29)



# [1.1.0](https://gitlab.com/openstapps/feed-importer/compare/v1.0.0...v1.1.0) (2021-09-02)



# [1.0.0](https://gitlab.com/openstapps/feed-importer/compare/v0.0.2...v1.0.0) (2020-11-30)


### Features

* add feed item transformation option ([b1cc3a4](https://gitlab.com/openstapps/feed-importer/commit/b1cc3a42fab6c8259817d57857fd3574dae968a1))



## [0.0.2](https://gitlab.com/openstapps/feed-importer/compare/v0.0.1...v0.0.2) (2018-12-03)



## [0.0.1](https://gitlab.com/openstapps/feed-importer/compare/f853e025de7468226bba30bbf3b721ab8c9335bc...v0.0.1) (2018-12-03)


### Features

* add feed-importer ([f853e02](https://gitlab.com/openstapps/feed-importer/commit/f853e025de7468226bba30bbf3b721ab8c9335bc))



